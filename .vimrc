syntax on
set tabstop=4
filetype on
filetype indent on
" set cin
set expandtab
set shiftwidth=4
set softtabstop=4
set tabstop=4
" set textwidth=79
set shiftround
set autoindent
color koehler
filetype plugin indent on

set scrolloff=0

inoremap " ""<left>
inoremap ' ''<left>
inoremap ( ()<left>
inoremap [ []<left>
inoremap { {}<left>
